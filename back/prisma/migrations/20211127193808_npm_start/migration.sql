-- CreateEnum
CREATE TYPE "GameStatus" AS ENUM ('RUNNING', 'LOST', 'COMPLETED');

-- CreateTable
CREATE TABLE "Game" (
    "id" TEXT NOT NULL,
    "username" TEXT NOT NULL,
    "width" INTEGER NOT NULL,
    "height" INTEGER NOT NULL,
    "duration" INTEGER NOT NULL,
    "status" "GameStatus" NOT NULL DEFAULT E'RUNNING',
    "board" JSONB[],
    "score" INTEGER NOT NULL DEFAULT 0,
    "startDate" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "endDate" TIMESTAMP(3)
);

-- CreateIndex
CREATE UNIQUE INDEX "Game_id_key" ON "Game"("id");
