import app from './routers';
import config from './config';
import database from './database';

// Connect to the database, then run the API on the configured port
database.$connect().then(() => {
  app.listen(config.app.port, () => {
    console.log(`API running on http://localhost:${config.app.port} ...`);
  });
}).catch((error) => {
  console.error(error);
});