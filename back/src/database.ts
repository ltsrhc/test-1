import { PrismaClient } from '@prisma/client';

import config from './config';

const database = new PrismaClient({
  errorFormat: 'pretty',
  rejectOnNotFound: true,
  datasources: {
    db: {
      url: config.database.url,
    }
  }
});

export default database;
