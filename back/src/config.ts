import env from 'env-var';

const config = {
  /** App configuration */
  app: {
    /** Port number on which the API will listen for requests */
    port: env.get('PORT').default('5000').asPortNumber(),
  },

  /** Database configuration */
  database: {
    /** Database url used by the API */
    url: env.get('DATABASE_URL').default('postgresql://user:yYKDF8Sdx@database:5432/dbtests?schema=public').asUrlString(),
  },

  /** Redis configuration */
  redis: {
    /** Redis hostname used to connect to the redis service */
    host: env.get('REDIS_HOST').default('localhost').asString(),
    /** Port on which the redis service listens */
    port: env.get('REDIS_PORT').default('6379').asPortNumber(),
    /** Password used to authenticate to the redis service */
    password: env.get('REDIS_PASSWORD').default('QJcphbM6L8Q').asString(),
  },
};

export default config;