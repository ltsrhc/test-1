import { RequestHandler } from 'express';
import { validate } from 'class-validator';
import { ClassConstructor, plainToClass } from 'class-transformer';

import { ValidationException } from '../utils/exception';

/** Middleware used to validate bodies and queries from incoming requests. */
function validationMiddleware(type: ClassConstructor<any>, where: string): RequestHandler {
  return async (req, _res, next) => {
    let obj;
    if (where === 'body') {
      req.body = plainToClass(type, req.body);
      obj = req.body;
    } else if (where === 'query') {
      req.query = plainToClass(type, req.query);
      obj = req.query;
    } else {
      throw new Error('Trying to use the request validation middleware on unknown field.');
    }

    const errors = await validate(obj, { forbidNonWhitelisted: true });
    if (errors.length !== 0) {
      const error = ValidationException(errors);
      return next(error);
    }
    return next();
  };
}

export const bodyMiddleware = (type: ClassConstructor<any>) => validationMiddleware(type, 'body');

export const queryMiddleware = (type: ClassConstructor<any>) => validationMiddleware(type, 'query');
