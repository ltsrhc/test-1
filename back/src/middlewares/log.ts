import express from 'express';

export function logMiddleware(req: express.Request, res: express.Response, next: () => void): void {
  const start = Date.now();
  const { url } = req;

  console.log(`request [${req.method}] ${url} sent from ${req.ip}`);

  res.on('finish', () => {
    const elapsed = Date.now() - start;
    console.log(`request [${req.method}] ${url} proceed on ${elapsed}ms with status ${res.statusCode}`);
  });

  return next();
}
