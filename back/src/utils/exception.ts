import { StatusCodes } from 'http-status-codes';

export interface ApiErrorConstructor {
  /**
   * An unique identifier to the error instance, such as user-0001
   */
  code: string;

  /**
   * The http status code of the error
   */
  status: number;

  /**
   * The main message of the error. It should explain the error shortly.
   */
  message: string;

  /**
   * Name of the exception, used to make debugging easier
   */
  type: string;

  /**
   * Any additional information that can be provided about the error
   */
  context?: unknown;
}

export function isApiError(error: unknown): error is ApiError {
  return error instanceof ApiError;
}

export class ApiError extends Error {
    code: string;
    status: number;
    message: string;
    type: string;
    context?: unknown;

    constructor(data: ApiErrorConstructor) {
      super(data.message);
      this.code = data.code;
      this.status = data.status;
      this.message = data.message;
      this.context = data.context;
      this.type = data.type;
    }

    toObject() {
      return {
        code: this.code,
        type: this.type,
        status: this.status,
        message: this.message,
        context: this.context,
      };
    }
}

export enum ExceptionCode {
  // User exceptions
  GameNotFound = 'game-0001',
  GameAlreadyCompleted = 'game-0002',

  // Validation exceptions
  ValidationException = 'valid-0001',

  // Misc exceptions
  InternalServerErrorException = 'misc-0001',
}

export function GameNotFound(context?: unknown) {
  return new ApiError({
    type: 'GameNotFound',
    status: StatusCodes.NOT_FOUND,
    code: ExceptionCode.GameNotFound,
    message: 'The targeted game was not found',
    context,
  });
}

export function GameAlreadyCompleted(context?: unknown) {
  return new ApiError({
    type: 'GameAlreadyCompleted',
    status: StatusCodes.BAD_REQUEST,
    code: ExceptionCode.GameAlreadyCompleted,
    message: 'The user related to this request is already verified',
    context,
  });
}

export function ValidationException(context?: unknown) {
  return new ApiError({
    type: 'ValidationException',
    status: StatusCodes.BAD_REQUEST,
    code: ExceptionCode.ValidationException,
    message: 'Invalid plain to class transformation',
    context,
  });
}

export function InternalServerErrorException(context?: unknown) {
  return new ApiError({
    type: 'InternalServerErrorException',
    status: StatusCodes.INTERNAL_SERVER_ERROR,
    code: ExceptionCode.InternalServerErrorException,
    message: 'Internal server error',
    context,
  });
}
