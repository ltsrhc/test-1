import cors from 'cors';
import express from 'express';
import { json } from 'body-parser';
import { StatusCodes } from 'http-status-codes';

import gameRouter from './game';
import { logMiddleware } from '../middlewares/log';

const app = express();

// ! Allow all origins, not prod ready
app.use(cors());
app.use(json());
app.use(logMiddleware);

// Used to check if API is up, especially useful for docker-compose healthcheck
app.get('/healthcheck', (_req, res) => {
  return res.sendStatus(StatusCodes.NO_CONTENT);
});

app.use('/game', gameRouter);

export default app;
