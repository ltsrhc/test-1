import { IsEnum, IsNumber, IsString, MaxLength, MinLength } from 'class-validator';
import { GameStatus } from '@prisma/client';

export class NewGameDto {
  @IsString()
  @MinLength(4)
  @MaxLength(64)
  username!: string;
}

export class GameActionDto {
  @IsString()
  @IsEnum(GameStatus)
  status!: GameStatus;

  @IsNumber()
  numberOfPairs!: number;
}
