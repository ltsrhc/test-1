export enum CardStatus {
  VISIBLE = 'VISIBLE', /// Card is still visibile
  HIDDEN = 'HIDDEN', /// Card is not shown anymore
}

export const BOARD_WIDTH = 4;
export const BOARD_HEIGHT = 4;
export const MAX_INDEX = BOARD_WIDTH * BOARD_HEIGHT;
