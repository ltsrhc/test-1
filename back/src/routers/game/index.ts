import express from 'express';
import asyncHandler from 'express-async-handler';
import { StatusCodes } from 'http-status-codes';

import database from '../../database';
import shuffle from '../../utils/shuffle';
import { GameActionDto, NewGameDto } from './dto';
import { bodyMiddleware } from '../../middlewares/validator';
import { GameAlreadyCompleted, GameNotFound } from '../../utils/exception';
import { BOARD_WIDTH, BOARD_HEIGHT, CardStatus, MAX_INDEX } from './constants';

const router = express.Router();

// Returns the 10 best scores from over games
router.get('/scores', asyncHandler(async (req, res) => {
  const scores = await database.game.findMany({
    take: 10,
    select: {
      username: true,
      score: true,
    },
    where: {
      status: {
        in: ['COMPLETED', 'LOST'],
      },
    },
    orderBy: {
      score: 'desc',
    }
  });

  res.status(StatusCodes.OK).send(scores);
}));

// Create a new game and return its id to the client
router.post('/new', bodyMiddleware(NewGameDto), asyncHandler(async (req, res) => {
  const { username } = req.body as NewGameDto;
  const randomSeed = Math.random();

  const board = [...Array(BOARD_WIDTH * BOARD_HEIGHT)].map((_v, i) => {
    const pairId = i + (i % 2 === 0 ? 1 : -1);
    const baseSeed = i % 2 === 0 ? `${i}${pairId}` : `${pairId}${i}`;

    return {
      id: i,
      status: CardStatus.VISIBLE, // All cards are displayed when the game starts
      pairId, // Pair cards 2 by 2
      background: `https://picsum.photos/seed/${baseSeed}${randomSeed}/200`, // card background
    };
  });
  const shuffledBoard = shuffle(board);

  const game = await database.game.create({
    data: {
      duration: 60,
      width: BOARD_WIDTH,
      height: BOARD_HEIGHT,
      username: username,
      board: shuffledBoard,
    }
  });

  res.status(StatusCodes.CREATED).send(game);
}));

router.put('/:id', bodyMiddleware(GameActionDto), asyncHandler(async (req, res) => {
  const data = req.body as GameActionDto;
  const timestamp = new Date();
  let game;

  try {
    game = await database.game.findUnique({
      where: { id: req.params.id },
    });
  } catch (error) {
    throw GameNotFound();
  }

  if (game.status === 'COMPLETED' || game.status === 'LOST') {
    throw GameAlreadyCompleted();
  }

  const gameDuration = timestamp.getTime() - game.startDate.getTime();
  const score = Math.round((data.numberOfPairs / (gameDuration / 1000)) * 100000)

  await database.game.update({
    where: { id: req.params.id },
    data: {
      status: data.status,
      score,
    },
  });

  res.sendStatus(StatusCodes.NO_CONTENT);
}));

export default router;
