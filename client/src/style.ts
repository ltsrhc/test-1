import styled from 'styled-components';

export const AppContainer = styled.div`
  display: flex;
  flex-direction: column;
  height: 100%;
  width: 100%;
  align-items: center;
  background-color: #282c34;
  text-align: center;
`;

export const ScoreContainer = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  text-align: center;
`;

export const ScoreText = styled.p`
  font-size: 1.75rem;
  color: white;
  font-weight: bold;
  margin: 15px;
`;

export const Grid = styled.div`
  margin-top: 20px;
  margin-bottom: 20px;
  display: flex;
  width: 850px;
  height: 850px;
  justify-content: center;
  flex-wrap: wrap;
`;

const GridItemContainer = styled.div`
  margin: 5px;
  border-radius: 5px;
  height: 200px;
  width: 200px;
  overflow: hidden;
  background: #f78587;
  box-shadow: 0px 0px 12px -2px #000000;
`;

export const GridItemActive = styled(GridItemContainer)`
  :hover {
    cursor: pointer;
  }
`;

export const GridItemInactive = styled(GridItemContainer)`
  visibility: hidden;
`;

export const PlayContainer = styled.div`
  display: flex;
  margin: 20px;
`;

export const Button = styled.button`
  font-size:15px;
  width:140px;
  height:50px;
  border-width:1px;
  color:#fff;
  border-color:#18ab29;
  font-weight:bold;
  border-top-left-radius:28px;
  border-top-right-radius:28px;
  border-bottom-left-radius:28px;
  border-bottom-right-radius:28px;
  text-shadow: 1px 1px 0px #2f6627;
  background:#44c767;
  :hover {
    cursor: pointer;
    background: rgba(21, 201, 6, 1);
  }
`;

export const TextInput = styled.input`
  padding: 8px;
  font-size: 28px;
  border-width: 1px;
  border-color: #0fff00;
  background-color: #FFFFFF;
  color: #3db235;
  border-style: solid;
  border-radius: 10px;
  box-shadow: 0px 0px 14px rgba(15,255,0,.75);
  text-shadow: 0px 0px 4px rgba(15,255,0,.75);
  :focus {
    outline:none;
  }
`;
