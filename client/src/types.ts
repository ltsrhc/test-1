export type Card = {
  id: number;
  status: string;
  pairId: number;
  background: string;
}

export type Score = {
  username: string;
  score: number;
}

export type Game = {
  id: string
  /**
   * Username related to this game
   */
  username: string
  /**
   * Horizontal number of cards
   */
  width: number
  /**
   * Vertical number of cards
   */
  height: number
  /**
   * Game maximum duration, in seconds
   */
  duration: number
  /**
   * Wether the user completed the game before time runs out
   */
  completed: boolean
  /**
   * Array representing the current state of the game board
   */
  board: Card[]
  /**
   * Date stored after the first move
   */
  startDate: Date | null
  /**
   * Date stored once the game is over
   */
  endDate: Date | null
}

export type GameBoardProps = {
  board: Game['board'];
  onPair: () => void;
};
