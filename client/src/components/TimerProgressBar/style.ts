import styled from 'styled-components';

export const Container = styled.div`
  height: 40px;
  width: 90%;
  border-radius: 5px;
  overflow: hidden;
  background: white;
  margin: 10px;
`;

export const Filler = styled.div`
  background: rgb(205,0,75);
  background: linear-gradient(90deg, rgba(205,0,75,1) 33.3%, rgba(243,113,27,1) 66.66%, rgba(252,184,8,1) 100%);
  height: 100%;
`;
