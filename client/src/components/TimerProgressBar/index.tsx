import React, { useState, useEffect } from 'react';

import { Container, Filler } from './style';

type TimerProgressBarProps = {
  /** Timer duration in seconds */
  duration: number,
  /** Called once the countdown is reached */
  callback?: () => void;
};

export function TimerProgressBar(props: TimerProgressBarProps) {
  const [deltaDate, setDeltaDate] = useState(new Date());
  const [duration, setDuration] = useState(props.duration);
  const [percent, setPercent] = useState(100);

  useEffect(() => {
    const timerId = setInterval(() => {
      if (duration <= 0) {
        if (props.callback) {
          props.callback();
        }
        setDuration(0);
        setPercent(0);
        clearInterval(timerId);
        return;
      }

      const now = new Date();
      const delta = (now.getTime() - deltaDate.getTime()) / 1000;
      setDeltaDate(now);
      setDuration(duration - delta);
      setPercent((duration / props.duration) * 100);
    }, 100);

    return () => clearInterval(timerId);
  });

  return (
    <Container>
      <Filler style={{ width: `${100 - percent}%` }} />
    </Container>
  );
}
