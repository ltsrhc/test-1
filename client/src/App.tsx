import React, { useEffect, useState } from 'react';
import axios from 'axios';
import ReactCardFlip from 'react-card-flip';

import { Card, Game, GameBoardProps, Score } from './types';
import { TimerProgressBar } from './components/TimerProgressBar';
import { Grid, GridItemInactive, GridItemActive, AppContainer, ScoreText, ScoreContainer, Button, TextInput, PlayContainer } from './style';

function GameBoard({ board, onPair }: GameBoardProps) {
  const [flippedCard1, setFlippedCard1] = useState<Card>(null);
  const [flippedCard2, setFlippedCard2] = useState<Card>(null);

  const handleClick = (card: Card) => {
    // If there are already 2 flipped cards, do nothing
    if (flippedCard1 && flippedCard2) {
      return;
    }

    if (!flippedCard1) {
      // Flip first card if not already flipped
      setFlippedCard1(card);
    } else if (!flippedCard2) {
      // If there is already a flipped card, flip a second one and check if
      // they make a pair. If so, remove them from the game and call onPair.
      // Finally, flip back both cards
      setFlippedCard2(card);
      setTimeout(() => {
        if (flippedCard1.id === card.pairId) {
          onPair();
          flippedCard1.status = 'HIDDEN';
          card.status = 'HIDDEN';
        }
        setFlippedCard1(null);
        setFlippedCard2(null);
      }, 500);
    }
  };

  const items = board.map((card) => {
    const isCard1Flipped = flippedCard1 && flippedCard1.id === card.id;
    const isCard2Flipped = flippedCard2 && flippedCard2.id === card.id;
    const isCardFlipped = card.status === 'VISIBLE' && (isCard1Flipped || isCard2Flipped);

    if (card.status === 'HIDDEN') {
      return <GridItemInactive key={card.id} />;
    }

    return (
      <ReactCardFlip isFlipped={isCardFlipped} key={card.id} flipDirection='horizontal'>
        <GridItemActive key={card.id} onClick={() => { handleClick(card) }} />
        <GridItemActive key={card.id} >
          {isCardFlipped &&
            <img src={card.background} />
          }
        </GridItemActive>
      </ReactCardFlip>
    );
  });

  return (
    <Grid>
      {items}
    </Grid>
  );
}

function App() {
  const [scoreRecords, setScores] = useState<Score[]>(null);
  const [username, setUsername] = useState<string>('');
  const [game, setGame] = useState<Game>(null);
  const [numberOfPairs, setNumberOfPairs] = useState(0);


  const fetchGame = async () => {
    if (!username || username.length < 4 || username.length > 64) {
      return;
    }

    try {
      const { data: game } = await axios.post<Game>('http://localhost:5000/game/new', {
        username,
      });
      setGame(game);
    } catch (error) {
      console.error(error);
    }
  };

  const fetchScores = async () => {
    try {
      const { data: scores } = await axios.get<Score[]>('http://localhost:5000/game/scores');
      setScores(scores);
    } catch (error) {
      console.error(error);
    }
  };

  const onPair = () => {
    setNumberOfPairs(numberOfPairs + 1);
    console.log(numberOfPairs);
    if (numberOfPairs + 1 === game.board.length / 2) {
      endGame('COMPLETED', numberOfPairs + 1);
    }
  };

  const handleUsernameChange = (e) => {
    setUsername(e.target.value);
  };

  const endGame = async (status: 'COMPLETED' | 'LOST', _numberOfPairs = 0) => {
    try {
      await axios.put(`http://localhost:5000/game/${game.id}`, {
        status,
        numberOfPairs: _numberOfPairs,
      });
      window.location.reload();
    } catch (error) {
      console.error(error);
    }
  }

  useEffect(() => {
    fetchScores();
  }, []);

  return (
    <AppContainer>
      {(game === null && scoreRecords) &&
        <>
          <ScoreText>Leaderboard</ScoreText>
          <ScoreContainer>
            {scoreRecords.map((sr, i) => (<ScoreText key={i}>{i + 1} - {sr.username} {sr.score} points</ScoreText>))}
          </ScoreContainer>
        </>
      }
      {game === null &&
        <PlayContainer>
          <TextInput type='text' value={username} name='username' onChange={handleUsernameChange}/>
          <div style={{ marginLeft: 20 }}/>
          <Button onClick={fetchGame}>Play</Button>
        </PlayContainer>
      }
      {game !== null &&
        <>
          <GameBoard board={game.board} onPair={onPair}/>
          <TimerProgressBar duration={game.duration} callback={() => endGame('LOST', numberOfPairs)}/>
        </>
      }
    </AppContainer>
  );
}

export default App;
